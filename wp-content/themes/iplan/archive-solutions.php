<?php
get_header();
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">


			<div class="solutions-header">
				<h1>
					Our Solutions
				</h1>
				<p>
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud.
				</p>
			</div>
			<div class="solutions-page">
			<?php
			$count = 0;
			$wpb_all_query = new WP_Query(array('post_type'=>'solutions', 'orderby' => 'menu_order', 'order'=> 'ASC', 'post_status'=>'publish', 'posts_per_page'=>-1)); ?>
				<?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post();
				$count++;
				$link = get_the_permalink();
				$title = get_the_title();
				$solIcon = get_field('solution_icon');
				
				echo '<a class="solutions-page--solution" href="' . $link . '">';
				echo '<span class="solutions-page--solution--title">' . $count . '. ' . $title . '</span>';
				echo '<img src="' . $solIcon['url'] . '">';
				//echo $count . '. ' . $title . ' - ' . $link . '<img src="' . $solIcon['url'] . '">';
				echo '</a>';
				
				get_template_part( 'template-parts/content/content', 'excerpt' );

				// End the loop.
			endwhile; ?>
			</div>
		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
