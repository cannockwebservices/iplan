<?php
/**
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage iPlan
 * @since 1.0.0
 */

function iplan_enqueue() {
	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/assets/js/jquery-3.3.1.min.js', null );
	wp_enqueue_script( 'customjs', get_template_directory_uri() . '/assets/fontawesome/js/all.min.js', null );
	wp_enqueue_script( 'fontawesomejs', get_template_directory_uri() . '/assets/js/custom.min.js', null );
	wp_enqueue_script( 'slickjs', get_template_directory_uri() . '/assets/js/slick.min.js', null );
	wp_enqueue_style( 'slick_styles', get_template_directory_uri() . '/assets/css/slick.css', null );
	wp_enqueue_style( 'slick_theme_styles', get_template_directory_uri() . '/assets/css/slick-theme.css', null );
	wp_enqueue_style( 'main_styles', get_template_directory_uri() . '/assets/css/styles.css', null );
	wp_enqueue_style( 'fontawesome_styles', get_template_directory_uri() . '/assets/fontawesome/css/all.min.css', null );
} 
add_action( 'wp_enqueue_scripts', 'iplan_enqueue' );

function iplan_theme_setup() {
register_nav_menus( array( 
	'main-nav' => 'Main Nav',
	//'footer-menu-1' => 'Footer Menu 1'
) );
}

function iplan_register_widgets() {
	register_sidebar( array(
		'name' => __( 'Footer Widget 1', 'iplan' ),
		'id' => 'footer-widget-1',
		'before_widget' => '<div class="footer-column-1 footer-columns--column">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="footer-columns--column--title">',
		'after_title' => '</h2>'
	));	
	register_sidebar( array(
		'name' => __( 'Footer Widget 2', 'iplan' ),
		'id' => 'footer-widget-2',
		'before_widget' => '<div class="footer-column-2 footer-columns--column">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="footer-columns--column--title">',
		'after_title' => '</h2>'
	));	
	register_sidebar( array(
		'name' => __( 'Footer Widget 3', 'iplan' ),
		'id' => 'footer-widget-3',
		'before_widget' => '<div class="footer-column-3 footer-columns--column">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="footer-columns--column--title">',
		'after_title' => '</h2>'
	));	
}
add_action( 'widgets_init', 'iplan_register_widgets' );
  
add_action( 'after_setup_theme', 'iplan_theme_setup' );

if( function_exists('acf_add_options_page') ) {

acf_add_options_page(array(
	'page_title' 	=> 'Theme General Settings',
	'menu_title'	=> 'Theme Settings',
	'menu_slug' 	=> 'theme-general-settings',
	'capability'	=> 'edit_posts',
	'redirect'		=> false
));

acf_add_options_sub_page(array(
	'page_title' 	=> 'Theme Header Settings',
	'menu_title'	=> 'Header',
	'parent_slug'	=> 'theme-general-settings',
));
	
}