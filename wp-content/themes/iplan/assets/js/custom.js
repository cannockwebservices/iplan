jQuery(document).ready(function(){
    jQuery(".mobile-menu-trigger").on("click", function(){
        jQuery(this).toggleClass("active");
        jQuery("nav").slideToggle();
    });

    jQuery('#home-solutions-bar--dropdown').on('change', function() {
        var locationValue = jQuery(this).val();
        jQuery('.home-solution-single').hide();
        jQuery('.home-solution-single.'+locationValue).show();
    });
    jQuery('.home-solution-single__solution--inner--close').on('click', function() {
        jQuery('#home-solutions-bar--dropdown').val('default').trigger('change');
    });
    jQuery('.home-hero--slider').slick({
        arrows: false,
        autoplay: true,
        autoplaySpeed: 5000
    });
});