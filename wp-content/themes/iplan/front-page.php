<?php
get_header();
?>

	<section id="primary" class="content-area">
			<main id="main" class="site-main">
			<?php while ( have_posts() ) :
					the_post(); ?>
				<div class="home-hero">
					<?php if( get_field('home_top_banner_content') == 'image' ): 
						$heroImage = get_field('home_top_banner_image');?>
					<div class="home-hero--image" style="background-image:url(<?php echo $heroImage['url']; ?>);">
						<div class="home-hero--image--text container">
							<?php the_field('home_top_banner_text'); ?>
						</div>
					</div>
					<?php endif; 
					if( get_field('home_top_banner_content') == 'slider' ): ?>
						<div class="home-hero--slider">
							<?php while ( have_rows('home_top_banner_slider') ) : the_row();
							$sliderImage = get_sub_field('home_top_banner_slider_images');?>
								<div class="home-hero--slider--slide" style="background-image:url(<?php echo $sliderImage['url']; ?>);">
									<div class="home-hero--slider--slide--text container">
										<?php the_sub_field('home_top_banner_slider_text'); ?>
									</div>
								</div>
							<?php endwhile; ?>
						</div>
					<?php endif; ?>
				</div>
			<?php endwhile; ?>

			<div class="home-solutions-bar">
				<span>How Does I-Plan De-risk Your Supply Chain?
					<select id="home-solutions-bar--dropdown">
						<option value="default">Choose a selection</option>

						<?php 
						$args = array(
							'post_type'=>'solutions', 
							'posts_per_page'=>-1
						);
						$wpb_all_query = new WP_Query($args); ?>
						
							<?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post();
							$link = get_the_permalink();
							$title = get_the_title();
							$excerpt = get_the_excerpt();?>
									<option value="<?php echo preg_replace('/\s*/', '', $title); ?>"><?php echo $title; ?></option>
						<?php endwhile; ?>
					</select>
				</span>
			</div>

			<?php   

				/* Start the Loop */
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/content/content', 'page' ); 
					?>
					<div class="home-solution-single default">
						<div class="container">
							<?php if (get_field('home_solutions_opening_text')){ ?>
								<div class="default--opening">
									<?php the_field('home_solutions_opening_text'); ?>
								</div>
							</div>
							<?php } ?>
							<div class="default--quaters">
								<div class="default--quaters--single default--quaters--single__1">
									<div class="default--quaters--single--icon">
									<?php 
										$image = get_field('home_solutions_icon_1');
										if( !empty($image) ): ?>
											<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
										<?php endif; ?>
									</div>
									<div class="default--quaters--single--text">
										<?php the_field('home_solutions_quarter_1'); ?>
									</div>
								</div>
								<div class="default--quaters--single default--quaters--single__2">
									<div class="default--quaters--single--icon">
									<?php 
										$image = get_field('home_solutions_icon_2');
										if( !empty($image) ): ?>
											<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
										<?php endif; ?>
									</div>
									<div class="default--quaters--single--text">
										<?php the_field('home_solutions_quarter_2'); ?>
									</div>
								</div>
								<div class="default--quaters--single default--quaters--single__3">
									<div class="default--quaters--single--icon">
									<?php 
										$image = get_field('home_solutions_icon_3');
										if( !empty($image) ): ?>
											<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
										<?php endif; ?>
									</div>
									<div class="default--quaters--single--text">
										<?php the_field('home_solutions_quarter_3'); ?>
									</div>
								</div>
								<div class="default--quaters--single default--quaters--single__4">
									<div class="default--quaters--single--icon">
									<?php 
										$image = get_field('home_solutions_icon_4');
										if( !empty($image) ): ?>
											<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
										<?php endif; ?>
									</div>
									<div class="default--quaters--single--text">
										<?php the_field('home_solutions_quarter_4'); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php
				endwhile; // End of the loop.
				$count = 0;
				while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post();
					$count ++;
					$link = get_the_permalink();
					$title = get_the_title();
					$excerpt = get_the_excerpt();?>
					<div class="home-solution-single home-solution-single__solution <?php echo preg_replace('/\s*/', '', $title); ?>" style="display:none;">
						<div class="home-solution-single--inner home-solution-single__solution--inner container">
							<div class="home-solution-single__solution--inner--close">
								<i class="fas fa-times"></i>
							</div>
							<h2><?php echo $count . '. ' . $title ?></h2>
							<?php 
							$image = get_field('solution_icon');
							if( !empty($image) ): ?>
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="home-solution-single__solution--inner--icon" />
							<?php endif; ?>
							<div class="home-solution-single__solution--inner--text">
								<?php the_field('solution_homepage_text'); ?>
							</div>
							<a href="<?php echo $link ?>" class="home-solution-single__solution--inner--more">Read more <i class="fas fa-chevron-right"></i></a>
							<p><?php echo $excerpt ?></p>
						</div>
					</div>
				<?php endwhile; 

				while ( have_posts() ) :
					the_post(); 
					$iplanbgimg = get_field('about_background');?>
					<div class="homeabout" style="background-image: url(<?php echo $iplanbgimg['url']; ?>)">
						<img src="/wp-content/themes/iplan/assets/img/iplancircle.svg" alt="I-Plan">
					</div>
					<?php
					$aboutText = get_field('about_text');
					if( !empty($aboutText) ):
						echo '<div class="about-text container">' . $aboutText . '</div>';
					endif; ?>

				<?php endwhile; ?>
				<h2 class="home-latest-articles--main-title">Latest Articles</h2>
				<div class="home-latest-articles container">
					

				<?php 
						$args = array(
							'post_type'=>'post', 
							'posts_per_page'=>2
						);
						$wpb_all_query = new WP_Query($args); ?>
						
							<?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post();
							$link = get_the_permalink();
							$featuredImage = get_field('blog_main_image');;
							$title = get_the_title();
							$excerpt = get_field('blog_excerpt'); ?>
							<div class="home-latest-articles--single">
								<div class="home-latest-articles--single--image" style="background-image: url(<?php echo $featuredImage['url']; ?>)">
								</div>
								<div class="home-latest-articles--single--title">
									<?php echo $title ?>
								</div>
								<div class="home-latest-articles--single--text">
									<?php echo $excerpt ?>
								</div>
								<a href="<?php echo $link; ?>" class="home-latest-articles--single--more">Read more <i class="fas fa-chevron-right"></i></a>
							</div>
						<?php endwhile; ?>
				</div>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
