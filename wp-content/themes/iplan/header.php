<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<header>
	<div class="head-top">
		<div class="head-top--left">
		<?php $logo = get_field('logo', 'option'); ?>
		<a href="<?php echo get_home_url() ?>">
			<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
		</a>
		</div>

		<div class="head-top--right">
			<div class="mobile-menu-trigger">
				<span id="bar1" class="mobile-menu-trigger--bars"></span>
				<span id="bar2" class="mobile-menu-trigger--bars"></span>
				<span id="bar3" class="mobile-menu-trigger--bars"></span>
			</div>
			<?php if( have_rows('header_button', 'option') ): 

			while( have_rows('header_button', 'option') ): the_row();

				$headerButton = get_sub_field('header_button_link');
				$headerButtonBgColour = get_sub_field('header_button_background_colour');
				$headerButtonBgHoverColour = get_sub_field('header_button_background_hover_colour');
				$headerButtonTextColour = get_sub_field('header_button_text_colour');
				$headerButtonTextHoverColour = get_sub_field('header_button_text_hover_colour'); ?>
				<a class="header-button" href="<?php echo $headerButton['url'] ?>" target="<?php echo $headerButton['target'] ?>"><?php echo $headerButton['title'] ?></a>	
				<style type="text/css">
					.header-button{
						background-color: <?php echo $headerButtonBgColour ?>; 
						color: <?php echo $headerButtonTextColour ?>;
					}
					.header-button:hover{
						background-color: <?php echo $headerButtonBgHoverColour ?>; 
						color: <?php echo $headerButtonTextHoverColour ?>;
					}
				</style>
			<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
	<nav>
		<?php wp_nav_menu( array( 'theme_location' => 'main-nav', 'menu_class' => 'nav-menu' ) ); ?>
	</nav>
	</header>

	<div id="content" class="site-content">
