	</div><!-- #content -->

	<footer class="site-footer">
		<div class="footer-columns">
            <?php if( is_active_sidebar( 'footer-widget-1' ) ) : ?>
                <?php dynamic_sidebar( 'footer-widget-1' ); ?>
            <?php endif; ?>
            <?php if( is_active_sidebar( 'footer-widget-2' ) ) : ?>
                <?php dynamic_sidebar( 'footer-widget-2' ); ?>
            <?php endif; ?>
            <?php if( is_active_sidebar( 'footer-widget-3' ) ) : ?>
                <?php dynamic_sidebar( 'footer-widget-3' ); ?>
            <?php endif; ?>
        </div>
        <div class="footer-contact">
            <?php 
            $telephone = get_field('telephone_number', 'option');
            if ($telephone) { ?> 
            <div class="footer-contact--telephone">
                <span>T </span><a href="tel:<?php echo str_replace([' ', '-'], '', $telephone);?>"><?php echo $telephone ?> </a>
            </div>
            <?php } ?>

            <?php 
            $email = get_field('email_address', 'option');
            if ($email) { ?> 
            <div class="footer-contact--email">
                <span>E </span><a href="mailto:<?php echo $email; ?>"><?php echo $email ?> </a>
            </div>
            <?php } ?>

        </div>
        <div class="footer-copy">
            <span>&copy; I-plan</span> <a href="">Privacy Policy</a> <a href="#">Contact Form</a>
        </div>
        <div class="footer-social">
            <?php
            $facebook = get_field('facebook_url', 'option');
            $twitter = get_field('twitter_url', 'option');
            $linkedin = get_field('linkedin_url', 'option');
            if ($facebook){ ?>
                <a href="<?php echo $facebook ?>"><i class="fab fa-facebook"></i></a>
            <?php } 
            if ($twitter){ ?>
                <a href="<?php echo $twitter ?>"><i class="fab fa-twitter"></i></a>
            <?php } 
            if ($linkedin){ ?>
                <a href="<?php echo $linkedin ?>"><i class="fab fa-linkedin-in"></i></a>
            <?php } ?>
        </div>
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
