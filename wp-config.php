<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'iplan');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ZW,1y-|J<dk/WX^a0m32lC9<gtk< >DGqRbjm%iB|{F?dHD6YM`P;km<zi?!HVWJ');
define('SECURE_AUTH_KEY',  '@%Tl z8cTBxaI{allUNf@0=*k?Y7WW*|ZK.1xCf~},Rj$907Epb&Y1hT&C9(tNo8');
define('LOGGED_IN_KEY',    'D|7n,~uJ?-?yWfjhbr#x]omT#IKwVf<KMMVrj-0|FwL~7D1A6EX.w|70=U/,pg4c');
define('NONCE_KEY',        'Bk56JONa*Xffo6BH{]~q.ox$#b%v.A({cT;N0<D%K8{^BA+L~RMc(L=%GV,}^E9>');
define('AUTH_SALT',        'cFrDg+UYx8RK@blBvqA02S?+iRVuL#JuV_[[P~%i?k$7s=eDkQG=<HUcD/S7)*j;');
define('SECURE_AUTH_SALT', 'WPV{-_([z6E)6LfgK,K4$d?2YzlUN5`+uKGWTYaMb?bbhS[),*q%V_[FoV)}Sz7l');
define('LOGGED_IN_SALT',   '5@9U~SvTJ),?Inf7Suproe^;(H-]}o>K6L{UVEbLEZh!|]Mlz,[v,;`v(`9*IrUl');
define('NONCE_SALT',       '^Pl-S0Dm&}D2wf=N_Zm*48/q%P7->;dPPg0]d]-gE[Bg~qRSrL=UsFSl{H3Kw ;A');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
